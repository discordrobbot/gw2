package colorsync

import (
	"fmt"

	"gitlab.com/MrGunflame/robbot/bot"
)

func unset(ctx *bot.Context) error {
	ch := ctx.Arguments.PopFirst()
	if ch.String() == "" {
		return ctx.InvalidCommandUsage()
	}

	// Extract channel id
	chID, err := getChannel(ctx.Session, ctx.Event.GuildID, ch)
	if err != nil {
		return err
	} else if chID == "" {
		ctx.Failure(fmt.Sprintf("Cannot find channel `%s`.", ch.String()))
		return nil
	}

	if _, err := deleteChannelColorLink(chID); err != nil {
		return err
	}

	ctx.Success(fmt.Sprintf("Successfully unlinked `%s`.", ch.String()))
	return nil
}

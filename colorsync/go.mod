module gitlab.com/discordrobbot/gw2/colorsync

go 1.15

require (
	github.com/bwmarrin/discordgo v0.22.0
	gitlab.com/MrGunflame/gw2api v0.2.5
	gitlab.com/MrGunflame/robbot v0.0.0-20201207232038-2646d1bb1c43
	golang.org/x/crypto v0.0.0-20201208171446-5f87f3452ae9 // indirect
	golang.org/x/sys v0.0.0-20201207223542-d4d67f95c62d // indirect
)

# Colorsync

The colorsync module allows for synchronization of Guild Wars 2 wvw side (colors) roles with channel permissions. E.G. all worlds that are red in their current matchup have access to channel #red.

## Installation

`./configure add https://gitlab.com/discordrobbot/gw2/colorsync`

package colorsync

import (
	"time"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/store"
)

const (
	permissionManage = "manage"
)

// Module colorsync allows to set channels to be accessed only by specific gw2 matchup sides
var Module = &bot.Module{
	Name:        "colorsync",
	Description: "Allow syncing channel permissions with Guild Wars 2 WvW sides",
	GuildOnly:   true,
	Commands: map[string]*bot.Command{
		"set": {
			Name:        "set",
			Description: "Set a channel to a color",
			Usage:       "<Channel> <Color>",
			Example:     "#red red",
			Execute:     set,
			Permissions: []string{permissionManage},
		},
		"unset": {
			Name:        "unset",
			Description: "Unset a channel as a color channel, making it a normal channel again",
			Usage:       "<Channel>",
			Example:     "#red",
			Execute:     unset,
			Permissions: []string{permissionManage},
		},
		"list": {
			Name:        "list",
			Description: "List all channels linked in this server.",
			Usage:       "",
			Example:     "",
			Execute:     list,
			Permissions: []string{permissionManage},
		},
		"sync": {
			Name:        "sync",
			Description: "Resynchronize all linked channels now.",
			Usage:       "",
			Example:     "",
			Execute:     sync,
			Permissions: []string{permissionManage},
		},
	},
	Tasks: []*bot.Task{
		{
			Name:      "refreshcolors",
			AtTime:    "19:00",
			AtWeekday: time.Friday,
			Execute:   refreshTask,
		},
	},
	Store: &store.Store{
		Tables: []*store.Table{
			{
				Name: "colorsync",
				Columns: []*store.Column{
					{
						Name:  "id",
						Type:  "INT",
						Flags: []store.SQLFlag{store.SQLFlagAutoIncrement, store.SQLFlagNotNull, store.SQLFlagPrimaryKey, store.SQLFlagUnsigned},
					},
					{
						Name:  "channel",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "guild",
						Type:  "VARCHAR",
						Len:   32,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
					{
						Name:  "color",
						Type:  "VARCHAR",
						Len:   16,
						Flags: []store.SQLFlag{store.SQLFlagNotNull},
					},
				},
			},
		},
	},
	Permissions: []string{permissionManage},
}

type channelColorLink struct {
	id      int
	channel string
	guild   string // Discord Guild
	color   string
}

func newChannelColorLink(channel, guild, color string) *channelColorLink {
	return &channelColorLink{
		channel: channel,
		guild:   guild,
		color:   color,
	}
}

func (d *channelColorLink) insert() (int, error) {
	return store.Insert("INSERT INTO colorsync (channel, guild, color) VALUES (?, ?, ?)", d.channel, d.guild, d.color)
}

func getChannelColorLinks(guild string) ([]*channelColorLink, error) {
	rows, err := store.Select("SELECT id, channel, color FROM colorsync WHERE guild = ?", guild)
	if err != nil {
		return nil, err
	}

	var links []*channelColorLink
	for rows.Next() {
		var l channelColorLink
		if err := rows.Scan(&l.id, &l.channel, &l.color); err != nil {
			return nil, err
		}
		links = append(links, &l)
	}

	return links, nil
}

func deleteChannelColorLink(channel string) (int, error) {
	return store.Delete("DELETE FROM colorsync WHERE guild = ?", channel)
}

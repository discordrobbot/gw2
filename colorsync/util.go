package colorsync

import (
	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
)

// getChannel tries to extract the value as a channel mention first
// If it doesn't match, it selects the first channel in the guild with the given name
// Returns an an empty string and nil when not found
func getChannel(s *discord.Session, gID string, arg *bot.Argument) (string, error) {
	if ch, ok := arg.ChannelID(); ok {
		return ch, nil
	}

	chs, err := s.S.GuildChannels(gID)
	if err != nil {
		return "", err
	}

	for _, c := range chs {
		if c.Name == arg.String() {
			return c.ID, nil
		}
	}

	return "", nil
}

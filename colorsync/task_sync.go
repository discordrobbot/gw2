package colorsync

import (
	"gitlab.com/MrGunflame/robbot/discord"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/MrGunflame/gw2api"
)

func refreshTask(s *discord.Session, guild string) error {
	// Get all linked channels from the database
	links, err := getChannelColorLinks(guild)
	if err != nil {
		return err
	}

	// Fetch all worlds from the gw2api
	worlds, err := gw2api.New().Worlds()
	if err != nil {
		return err
	}

	matches, err := gw2api.New().WvWMatches()
	if err != nil {
		return err
	}

	// worldColorMap contains the names of worlds that currently have the color of their key
	worldColorMap := make(map[string][]string)
	for _, m := range matches {
		for k, v := range m.AllWorlds {
			for _, x := range v {
				for _, w := range worlds {
					if w.ID == x {
						worldColorMap[k] = append(worldColorMap[k], w.Name)
					}
				}
			}
		}
	}

	guildRoles := make(map[string][]string)
	roles, err := s.S.GuildRoles(guild)
	if err != nil {
		return err
	}

loop:
	for _, r := range roles {
		for k, v := range worldColorMap {
			for _, x := range v {
				if r.Name == x {
					guildRoles[k] = append(guildRoles[k], r.ID)
					continue loop
				}
			}
		}
	}

	for _, l := range links {
		ch, err := s.S.Channel(l.channel)
		if err != nil {
			return err
		}

		if _, err := s.S.ChannelEditComplex(l.channel, &discordgo.ChannelEdit{
			PermissionOverwrites: makeOverwrites(ch, guildRoles, l.color),
		}); err != nil {
			return err
		}
	}

	return nil
}

func makeOverwrites(ch *discordgo.Channel, guildRoles map[string][]string, color string) []*discordgo.PermissionOverwrite {
	var overwrites []*discordgo.PermissionOverwrite

	for clr, ids := range guildRoles {
		if clr == color {
			for _, id := range ids {
				overwrites = append(overwrites, &discordgo.PermissionOverwrite{
					Type:  "role",
					ID:    id,
					Allow: discordgo.PermissionViewChannel + discordgo.PermissionVoiceConnect,
				})
			}
		} else {
			for _, id := range ids {
				overwrites = append(overwrites, &discordgo.PermissionOverwrite{
					Type: "role",
					ID:   id,
					Deny: discordgo.PermissionViewChannel + discordgo.PermissionVoiceConnect,
				})
			}
		}
	}

	return overwrites
}

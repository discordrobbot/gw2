package gw2mists

import (
	"fmt"
	"math"
	"sort"
	"strconv"
	"time"

	"gitlab.com/MrGunflame/robbot/bot"
	"gitlab.com/MrGunflame/robbot/discord"
	"gitlab.com/MrGunflame/robbot/pkg/util"

	"github.com/bwmarrin/discordgo"
)

var leaderboardCache = util.NewCache(1800 * time.Second)

func leaderboard(ctx *bot.Context) error {
	// var scope string
	// switch ctx.Arguments.PopFirst().String() {
	// case "week":
	// 	scope = "week"
	// case "total", "all":
	// 	scope = "total"
	// default:
	// 	scope = "week"
	// }

	page := ctx.Arguments.PopFirst().String()

	fmt.Println(page)
	_, err := strconv.Atoi(page)
	if err != nil {
		ctx.Failure(fmt.Sprintf("`%s` is not a valid number.", page))
		return nil
	}

	// When the requested page number is too high
	// if pageID > leaderboardCache.Len() {
	// 	ctx.Failure(fmt.Sprintf("Cannot find page %s.", page))
	// 	return nil
	// }

	// Try to serve from cache
	if val, ok := leaderboardCache.Get(page); ok {
		ctx.RespondEmbed(val.(*discord.Embed))
		return nil
	}

	var data leaderboardData
	if err := getEndpoint("leaderboard/player/v2", &data); err != nil {
		return err
	}

	// Sort by kills
	sort.Sort(data)

	for p := 0; p < int(math.Ceil(float64(len(data))/float64(50))); p++ {
		embed := &discord.Embed{
			Fields: []*discordgo.MessageEmbedField{},
			Footer: discord.DefaultEmbedFooter(util.ConcatStrings("https://gw2mists.com/leaderboards/player | Updated ", discord.FormatTimeDefault(time.Now().UTC()))),
		}
		for i := 0; i < 24 && i+24*p < len(data); i++ {
			d := data[i+24*p]
			embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
				Name:   util.ConcatStrings("**__[", strconv.Itoa((i+24*p)+1), "]__**__", d.AccountName, "__"),
				Value:  strconv.Itoa(d.Kills),
				Inline: true,
			})
		}

		leaderboardCache.Insert(strconv.Itoa(p+1), embed)
	}

	if val, ok := leaderboardCache.Get(page); ok {
		ctx.RespondEmbed(val.(*discord.Embed))
	} else {
		ctx.Failure(fmt.Sprintf("Page %s was not found.", page))
	}
	return nil
}

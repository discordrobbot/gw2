package gw2mists

import "gitlab.com/MrGunflame/robbot/bot"

// Module gm gives access to the gw2mists.de api
var Module = &bot.Module{
	Name:        "gm",
	Description: "Directly access gw2mists.de data from discord.",
	GuildOnly:   false,
	Commands: map[string]*bot.Command{
		"leaderboard": {
			Description: "List gw2mists.de leaderboards",
			Usage:       "",
			Example:     "",
			Execute:     leaderboard,
		},
	},
}

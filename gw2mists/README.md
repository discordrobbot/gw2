# GW2Mists

The gw2mists module provides integration tools for [GW2Mists](https://gw2mists.com), a website focused around WvW.

## Installation

`./configure add https://gitlab.com/discordrobbot/gw2/gw2mists`

## Features 

- Leaderboards (Kills only currently)

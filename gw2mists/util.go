package gw2mists

import (
	"encoding/json"
	"net/http"

	"gitlab.com/MrGunflame/robbot/pkg/util"
)

type leaderboardData []*leaderboardNode

// Single node for https://api.gw2mists.de/leaderboard/player/v2
type leaderboardNode struct {
	MaxKills           int    `json:"maxKills"`
	MaxKilledDolyaks   int    `json:"maxKilledDolyaks"`
	MaxEscortedDolyaks int    `json:"maxEscortedDolyaks"`
	MaxCapturedTargets int    `json:"maxCapturedTargets"`
	MaxDefendedTargets int    `json:"maxDefendedTargets"`
	MaxCapturedCamps   int    `json:"maxCapturedCamps"`
	MaxDefendedCamps   int    `json:"maxDefendedCamps"`
	MaxCapturedTowers  int    `json:"maxCapturedTowers"`
	MaxDefendedTowers  int    `json:"maxDefendedTowers"`
	MaxCapturedSM      int    `json:"maxCapturedSM"`
	MaxDefendedSM      int    `json:"maxDefendedSM"`
	Kills              int    `json:"kills"`
	KilledDolyaks      int    `json:"killedDolyaks"`
	EscortedDolyaks    int    `json:"escortedDolyask"`
	CapturedTargets    int    `json:"capturedTargets"`
	DefendedTargets    int    `json:"defendedTargets"`
	CapturedCamps      int    `json:"capturedCamps"`
	DefendedCamps      int    `json:"defendedCamps"`
	CapturedTowers     int    `json:"capturedTowers"`
	DefendedTowers     int    `json:"defendedTowers"`
	CapturedKeeps      int    `json:"capturedKeeps"`
	DefendedKeeps      int    `json:"defendedKeeps"`
	CapturedSM         int    `json:"captured_sm"`
	DefendedSM         int    `json:"defendedSM"`
	WvWRank            int    `json:"wvwRank"`
	MaxWvWRank         int    `json:"maxWvwRank"`
	AccountName        string `json:"accountName"`
	WorldID            int    `json:"world_id"`
	WorldName          string `json:"worldName"`
	GuildID            string `json:"guildId"`
	GuildName          string `json:"guildName"`
	GuildTag           string `json:"guildTag"`
	GuildPublished     int    `json:"guildPublished"`
	GuildWorldID       int    `json:"guildWorldId"`
	GuildFlag          string `json:"guildFlag"`
	ProfileImage       string `json:"profile_image"`
	Tier               int    `json:"tier"`
	NameColor          string `json:"nameColor"`
	Points             int    `json:"points"`
	MaxPoints          int    `json:"maxPoints"`
}

func (d leaderboardData) Len() int {
	return len(d)
}

func (d leaderboardData) Less(i, j int) bool {
	return (d)[i].Kills > (d)[j].Kills
}

func (d leaderboardData) Swap(i, j int) {
	(d)[i], (d)[j] = (d)[j], (d)[i]
}

func getEndpoint(url string, dst interface{}) error {
	resp, err := http.Get(util.ConcatStrings("https://api.gw2mists.com/", url))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&dst); err != nil {
		return err
	}

	return nil
}
